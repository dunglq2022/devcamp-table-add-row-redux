import * as React from 'react';
import { styled } from '@mui/material/styles';
import { Grid, Container, TextField, Button, TableRow, TableContainer, TableHead, Table, TableBody, Paper } from "@mui/material";
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { useDispatch, useSelector } from 'react-redux';

const StyledTableCell = styled(TableCell)({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: 'gray',
    color: 'white',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
});

const StyledTableRow = styled(TableRow)({
  '&:nth-of-type(odd)': {
    backgroundColor: '#dee2e6',
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
});

function createData (stt, content) {
    return { stt, content };
}

function Filter () {
    const dispatch = useDispatch();

    const {inputChange, rows} = useSelector((reduxData) => reduxData.filterReducer)
    console.log(rows)
    const inputChangeHandler = event => {
        dispatch({
            type: 'VALUE_HANDLER',
            payload: {
                inputChange: event.target.value,
            }
        })
    }

    const addRowHandler = () => {
        //Tạo thêm 1 dữ liệu mới
        const newRow = createData(rows.length + 1, inputChange);
        const updateRow = [...rows, newRow]
        dispatch({
            type: 'UPDATE_ROWS',
            payload: {
                updateRow
            }
        });
    };


    return(
        <Container maxWidth="lg">
            <h1> Task 522.20 Filter </h1>
            <Grid container spacing={3} className="mt-3" style={{display: 'flex', alignItems: 'center'}}>
                <Grid item xs={3} className="mb-4">
                    <h4>Nhập nội dung dòng</h4>
                </Grid>
                <Grid item xs={6} className="mb-4">
                    <TextField style={{width: '100%'}} variant="outlined" value={inputChange} onChange={inputChangeHandler} />
                </Grid>
                <Grid item xs={3} className="mb-4">
                    <Button onClick={addRowHandler} style={{fontWeight: 'bolder', fontSize: 16, width: 150}} variant="outlined" color="info">Thêm</Button>
                </Grid>
            </Grid>
            <TableContainer style={{width: '100%'}} component={Paper} className="mt-4">
                <Table aria-label="simple table">
                    <TableHead>
                    <StyledTableRow>
                        <StyledTableCell>Stt</StyledTableCell>
                        <StyledTableCell>Nội Dung</StyledTableCell>
                    </StyledTableRow>
                    </TableHead>
                    <TableBody>
                    {rows.map((row, index) => (
                        <StyledTableRow key={index}>
                            <StyledTableCell>{row.stt}</StyledTableCell>
                            <StyledTableCell>{row.content}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                    </TableBody>
                </Table>
            </TableContainer>
       </Container> 
    )
}

export default Filter;