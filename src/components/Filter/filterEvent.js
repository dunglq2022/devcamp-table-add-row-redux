
const initialState = {
    inputChange: '', //Giá trị của ô input
    rows: [] //Danh sách dữ liệu
};


const filterEvent = (state = initialState, action) => {
    switch (action.type) {
        case "VALUE_HANDLER": {
            return {
                ...state,
                inputChange: action.payload.inputChange
            };
        }
        
        case "UPDATE_ROWS": {
            return {
                ...state,
                rows: action.payload.updateRow,
            }
        }
        default: {
            return state;
        }
    }
};

export default filterEvent;
