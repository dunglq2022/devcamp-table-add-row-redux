import { combineReducers, createStore } from "redux";
import filterEvent from '../components/Filter/filterEvent'

const appReducer = combineReducers({
    filterReducer: filterEvent
});

const store = createStore(
    appReducer,
);

export default store;